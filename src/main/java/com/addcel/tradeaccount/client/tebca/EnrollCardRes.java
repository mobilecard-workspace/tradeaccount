package com.addcel.tradeaccount.client.tebca;

import com.addcel.tradeaccount.spring.model.McResponse;

public class EnrollCardRes extends McResponse{

	private String pan;
	private String vigencia;
	
	public EnrollCardRes() {
		// TODO Auto-generated constructor stub
	}

	public String getPan() {
		return pan;
	}

	public void setPan(String pan) {
		this.pan = pan;
	}

	public String getVigencia() {
		return vigencia;
	}

	public void setVigencia(String vigencia) {
		this.vigencia = vigencia;
	}
	
}
