package com.addcel.tradeaccount.spring.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.addcel.tradeaccount.spring.model.CardReq;
import com.addcel.tradeaccount.spring.model.CommerceCard;
import com.addcel.tradeaccount.spring.services.TradeService;
import com.addcel.tradeaccount.spring.model.AccountUpdate;


@RestController
public class TradeController {
	
	private static final String PATH_GET_COMMERCE_ACCOUNT =  "{idApp}/{idPais}/{idioma}/CommerceCard";
	private static final String PATH_ACTIVATE_CARD_TEBCA = "{idApp}/{idPais}/{idioma}/Activate";
	private static final String PATH_CREATE_TEBCA_CARD = "{idApp}/{idPais}/{idioma}/Applyforcard";
	private static final String PATH_LOCK_UNLOCK_TEBCA_CARD = "{idApp}/{idPais}/{idioma}/chgblockstatus/{process}";
	private static final String PATH_ACCOUNT_REPLACEMENT = "{idApp}/{idPais}/{idioma}/replacement";
	private static final String PATH_ACCOUNT_UPDATE = "{idApp}/{idPais}/{idioma}/account/update";
	private static final String PATH_ACCOUNT_UPDATE_FAVORITE = "{idApp}/{idPais}/{idioma}/account/favorite";
	
	//CHECAR QUE CUANDO AGREGE UNA TARJETA O CUENTA, PONERLA COMO FAVORITA
	
	@Autowired
	TradeService service;
	
	@RequestMapping(value = "saludo", method=RequestMethod.GET, produces = "application/json")
	public ResponseEntity<Object> updateUserApto(){
			return  new ResponseEntity<Object>(service.getSaludo(),HttpStatus.OK);
		
	}
	
	@RequestMapping(value = PATH_GET_COMMERCE_ACCOUNT, method=RequestMethod.POST, produces = "application/json")
	public ResponseEntity<CommerceCard> getInfoCommerce(@RequestParam("id") long id,@PathVariable int idPais,@PathVariable String idioma,@PathVariable int idApp){
		if(service.isActive(idApp)){
			return  new ResponseEntity<CommerceCard>(service.getCommerceCard(id, idApp, idPais, idioma),HttpStatus.OK);
		}else{
			CommerceCard error = new CommerceCard();
			error.setIdError(401);
			error.setMensajeError("No estas autorizado para realizar la operacion");
			return  new ResponseEntity<CommerceCard>(error,HttpStatus.UNAUTHORIZED);
		}
	}
	
	@RequestMapping(value = PATH_ACTIVATE_CARD_TEBCA, method=RequestMethod.POST, produces = "application/json")
	public ResponseEntity<CommerceCard> ActivateCard(@RequestBody CardReq req,@PathVariable int idPais,@PathVariable String idioma,@PathVariable int idApp){
		if(service.isActive(idApp)){
			return  new ResponseEntity<CommerceCard>(service.ActivateCard(req, idApp, idPais, idioma),HttpStatus.OK);
		}else{
			CommerceCard error = new CommerceCard();
			error.setIdError(401);
			error.setMensajeError("No estas autorizado para realizar la operacion");
			return  new ResponseEntity<CommerceCard>(error,HttpStatus.UNAUTHORIZED);
		}
	}
	
	@RequestMapping(value = PATH_CREATE_TEBCA_CARD, method=RequestMethod.POST, produces = "application/json")
	public ResponseEntity<CommerceCard> ActivateCard(@RequestParam("id") long id ,@PathVariable int idPais,@PathVariable String idioma,@PathVariable int idApp){
		if(service.isActive(idApp)){
			return  new ResponseEntity<CommerceCard>(service.ApplyforCard(id, idApp, idPais, idioma),HttpStatus.OK);
		}else{
			CommerceCard error = new CommerceCard();
			error.setIdError(401);
			error.setMensajeError("No estas autorizado para realizar la operacion");
			return  new ResponseEntity<CommerceCard>(error,HttpStatus.UNAUTHORIZED);
		}
	}
	
	@RequestMapping(value = PATH_LOCK_UNLOCK_TEBCA_CARD, method=RequestMethod.POST, produces = "application/json")
	public ResponseEntity<CommerceCard> CardLockUnLock(@RequestParam("id") long id ,@PathVariable int process,@PathVariable int idPais,@PathVariable String idioma,@PathVariable int idApp){
		if(service.isActive(idApp)){
			return  new ResponseEntity<CommerceCard>(service.CardLockUnLock(id, idApp, idPais, idioma, process),HttpStatus.OK);
		}else{
			CommerceCard error = new CommerceCard();
			error.setIdError(401);
			error.setMensajeError("No estas autorizado para realizar la operacion");
			return  new ResponseEntity<CommerceCard>(error,HttpStatus.UNAUTHORIZED);
		}
	}
	
	@RequestMapping(value = PATH_ACCOUNT_REPLACEMENT, method=RequestMethod.POST, produces = "application/json")
	public ResponseEntity<CommerceCard> CardReplacement(@RequestParam("id") long id ,@PathVariable int idPais,@PathVariable String idioma,@PathVariable int idApp){
		if(service.isActive(idApp)){
			return  new ResponseEntity<CommerceCard>(service.CardReplacement(id, idApp, idPais, idioma),HttpStatus.OK);
		}else{
			CommerceCard error = new CommerceCard();
			error.setIdError(401);
			error.setMensajeError("No estas autorizado para realizar la operacion");
			return  new ResponseEntity<CommerceCard>(error,HttpStatus.UNAUTHORIZED);
		}
	}
	
	@RequestMapping(value = PATH_ACCOUNT_UPDATE, method=RequestMethod.POST, produces = "application/json")
	public ResponseEntity<CommerceCard> AccountUpdate(@RequestBody AccountUpdate account ,@PathVariable int idPais,@PathVariable String idioma,@PathVariable int idApp){
		if(service.isActive(idApp)){
			return  new ResponseEntity<CommerceCard>(service.AccountUpdate(account, idApp, idPais, idioma),HttpStatus.OK);
		}else{
			CommerceCard error = new CommerceCard();
			error.setIdError(401);
			error.setMensajeError("No estas autorizado para realizar la operacion");
			return  new ResponseEntity<CommerceCard>(error,HttpStatus.UNAUTHORIZED);
		}
	}
	
	@RequestMapping(value = PATH_ACCOUNT_UPDATE_FAVORITE, method=RequestMethod.POST, produces = "application/json")
	public ResponseEntity<CommerceCard> AccountFavoriteUpdate(@RequestParam("type") Integer type,@RequestParam("id") Integer id ,@PathVariable int idPais,@PathVariable String idioma,@PathVariable int idApp){
		if(service.isActive(idApp)){
			return  new ResponseEntity<CommerceCard>(service.AccountFavoriteUpdate(id, type, idApp, idPais, idioma),HttpStatus.OK);
		}else{
			CommerceCard error = new CommerceCard();
			error.setIdError(401);
			error.setMensajeError("No estas autorizado para realizar la operacion");
			return  new ResponseEntity<CommerceCard>(error,HttpStatus.UNAUTHORIZED);
		}
	}
	
	

}
