package com.addcel.tradeaccount.spring.config;

import java.security.KeyStore;
import java.sql.SQLException;

import org.mybatis.spring.SqlSessionFactoryBean;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.servlet.config.annotation.DefaultServletHandlerConfigurer;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import org.springframework.web.servlet.view.InternalResourceViewResolver;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;
import org.springframework.http.client.ClientHttpRequestFactory;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.jdbc.datasource.SimpleDriverDataSource;
import org.springframework.jndi.JndiObjectFactoryBean;
import org.apache.http.conn.ssl.AllowAllHostnameVerifier;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.conn.ssl.SSLContextBuilder;
import org.apache.http.conn.ssl.TrustSelfSignedStrategy;
import org.apache.http.conn.ssl.X509HostnameVerifier;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.ibatis.datasource.pooled.PooledDataSource;

import javax.naming.NamingException;
import javax.sql.DataSource;

@Configuration
@EnableWebMvc
@ComponentScan(basePackages = "com.addcel.tradeaccount.spring")
@MapperScan("com.addcel.tradeaccount.mybatis.model.mapper")
public class AppConfig extends WebMvcConfigurerAdapter{
	
     /**
      * Crear Datasource para la conexion
      * @return DataSource
      */
    public DataSource dataSource(){
		JndiObjectFactoryBean dsource = new JndiObjectFactoryBean();
		 dsource.setJndiName("java:/MobilecardDS");
		 dsource.setExpectedType(DataSource.class);
			try {
				dsource.afterPropertiesSet();
			} catch (IllegalArgumentException | NamingException e) {
				e.printStackTrace();
				throw new RuntimeException(e);
			}
			return (DataSource) dsource.getObject();
	 }
    
	
	  /**
	   *  Crear DataSourceTransactionManager a partir del datasource
	   * @return DataSourceTransactionManager
	   */
	  @Bean
       public DataSourceTransactionManager transactionManager()
	    {
	        return new DataSourceTransactionManager(dataSource());
	    }
	     /**
	      * Crear sessionFactory
	      * @return SqlSessionFactoryBean
	      * @throws Exception
	      */
	    @Bean
	    public SqlSessionFactoryBean sqlSessionFactoryBean() throws Exception{
	        SqlSessionFactoryBean sessionFactory = new SqlSessionFactoryBean();
	        sessionFactory.setDataSource(dataSource());        
	        // mybatis mapper
	        sessionFactory.setMapperLocations(new PathMatchingResourcePatternResolver().getResources("classpath:mapper/*.xml"));
	        sessionFactory.setTypeAliasesPackage("com.addcel.tradeaccount.mybatis.model.vo");
	        
	        return sessionFactory;
	    }
	    
	    
	    @Bean
	    public InternalResourceViewResolver jspViewResolver() {
	        InternalResourceViewResolver bean = new InternalResourceViewResolver();
	        bean.setPrefix("/WEB-INF/views/");
	        bean.setSuffix(".jsp");
	        return bean;
	    }
	    
	   /* @Override
	    public void addResourceHandlers(ResourceHandlerRegistry registry) {
	        registry.addResourceHandler("/resources/**").addResourceLocations("/resources/");
	    }*/
	   
	   /*@Override
		public void addResourceHandlers(final ResourceHandlerRegistry registry) {
			//registry.addResourceHandler("/js/**").addResourceLocations("/ui/js/");
			registry.addResourceHandler("/css/**").addResourceLocations("/resources/core/style/");
		}*/

	    
	 
	    
}
