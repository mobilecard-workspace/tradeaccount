package com.addcel.tradeaccount.spring.services;

import java.util.ArrayList;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.addcel.tradeaccount.client.tebca.BalanceRes;
import com.addcel.tradeaccount.client.tebca.EnrollCardRes;
import com.addcel.tradeaccount.mybatis.model.mapper.Mapper;
import com.addcel.tradeaccount.mybatis.model.vo.Bank;
import com.addcel.tradeaccount.mybatis.model.vo.CardCommerce;
import com.addcel.tradeaccount.mybatis.model.vo.Commerce;
import com.addcel.tradeaccount.spring.model.AccountCommerce;
import com.addcel.tradeaccount.spring.model.CardReq;
import com.addcel.tradeaccount.spring.model.CommerceCard;
import com.addcel.tradeaccount.spring.model.McResponse;
import com.addcel.tradeaccount.spring.model.MobileCardCard;
import com.addcel.tradeaccount.spring.model.MobilecardCardMovement;
import com.addcel.tradeaccount.spring.model.AccountUpdate;
import com.addcel.utils.AddcelCrypto;
import com.google.gson.Gson;


@Service
public class TradeServiceImp implements TradeService{

	private static final Logger LOGGER = LoggerFactory.getLogger(TradeServiceImp.class);
	
	@Autowired
	Mapper mapper;
	
	@Autowired
	TebcaClient tebcaClient;
	
	private Gson gson = new Gson();
	
	@Override
	public String getSaludo() {
		
		return "HOLA MUNDO";
	}
	
	@Override
	public boolean isActive(Integer idApp){
		boolean active = true;
		String band = null;
		
		band = mapper.isAppActive(idApp);
		if(band != null)
			if(band.equals("T"))
				active = true;
			else
				active = false;
		
		if(active)
			LOGGER.debug("SERVICIO AUTORIZADO PARA ID {}", idApp);
		else
			LOGGER.debug("SERVICIO DENEGADO PARA ID {}", idApp);
		return active;
	}
	
	@Override
	public CommerceCard getCommerceCard(long id, int idApp, int idPais, String idioma){
		CommerceCard card = new CommerceCard();
		MobileCardCard tebca = null;
		AccountCommerce cci = null;
		/**
		 * commerce.getCuenta_favorito() = 1 ---> cuenta clabe favorita
		 * commerce.getCuenta_favorito() = 2 ---> tarjeta tebca favorita 
		 */
		try{
			CardCommerce cardcommerce = mapper.getCardCommercebyCommerce(id, idApp);
			Commerce commerce = mapper.getCommerce(id, idApp);
			if(cardcommerce != null){
				
				if(cardcommerce.getTebca() == 1){ //tiene tarjeta tebca
					tebca = new MobileCardCard();
					tebca.setPan(AddcelCrypto.encryptHard(UtilsService.getSMS(cardcommerce.getNumero_tarjeta())));
					tebca.setCodigo(AddcelCrypto.encryptHard(UtilsService.getSMS(cardcommerce.getCt())));
					tebca.setVigencia(AddcelCrypto.encryptHard(UtilsService.getSMS(cardcommerce.getVigencia())));
					tebca.setMobilecard(true);
					tebca.setMovements(new ArrayList<MobilecardCardMovement>());
					tebca.setIdTarjeta( Long.valueOf(cardcommerce.getId_tarjeta()).intValue());
					tebca.setNombre(cardcommerce.getNombre_tarjeta());
					tebca.setFavorito(commerce.getCuenta_favorito() == 2);
					tebca.setEstatus(cardcommerce.getEstatus_tebca());//bloqueado 0 / desbloqueado 1
					tebca.setImg_short("https://www.mobilecard.mx/images/cards/short/Mobilecard.png");
					tebca.setImg_full("https://www.mobilecard.mx/images/cards/full/Mobilecard.png");
					tebca.setBalance(getBalanceTebca(id, idApp, idPais, idioma, "COMERCIO"));
					
				}
			}
			
			if(commerce != null){
				
				if(commerce.getCci() != null && !commerce.getCci().isEmpty()){//tiene cuenta clabe
					cci = new AccountCommerce();
					cci.setCuenta(commerce.getCci());
					cci.setNombre(commerce.getRepresentante_nombre() + " " + commerce.getRepresentante_paterno() + " " +commerce.getRepresentante_materno());
					cci.setIdBanco(commerce.getId_banco());
					cci.setFavorito(commerce.getCuenta_favorito() == 1);
					Bank bank = mapper.getBancoPeru(commerce.getId_banco());
					if(bank!= null)
						cci.setNombreBanco(bank.getNombre_corto());
				}
			}
			
			if( commerce == null){
				card.setIdError(300);
				card.setMensajeError("No se encontro comercio afiliado a Mobilecard");
			}else
			{
				card.setIdError(0);
				card.setMensajeError("");
			}
			
			
		}catch(Exception ex){
			LOGGER.error("ERROR AL OBTENER TARJETA MOBILECAR COMERCIO " + id , ex);
			card.setIdError(800);
			card.setMensajeError("Error al obtener cuenta comercio");
		}
		card.setTebca(tebca);
		card.setCci(cci);
		return card;
	}
	
	
	@Override
	public CommerceCard ActivateCard(CardReq req, int idApp, int idPais,
			String idioma) {
		CommerceCard card = new CommerceCard();
		int idError= 0;
		String MessageError = "";
		try{
			LOGGER.debug("*************ACTIVACION DE TARJETA {} {} *************",req.getId(),idPais);
			EnrollCardRes activation = tebcaClient.AssociateCard(req.getId(), req.getPan(), req.getVigencia(), req.getCodigo(), idApp, idPais, idioma, "COMERCIO");
			
			if(activation.getIdError() != 0){
				idError = activation.getIdError();
				MessageError = activation.getMensajeError();
			}
			LOGGER.debug("*********FINALIZANDO ACTIVACION {} {}**************",req.getId(),idPais);
			
		}catch(Exception ex){
			LOGGER.error("ERROR AL ACTIVAR TARJETA MOBILECAR " + req.getId(), ex);
		}finally{
			card = getCommerceCard(req.getId(), idApp, idPais, idioma);
			card.setIdError(idError);
			card.setMensajeError(MessageError);
		}
		LOGGER.debug("RETORNANDO RESPUESTA " + gson.toJson(card));
		return card;
	}
	
	@Override
	public CommerceCard ApplyforCard(long id, int idApp, int idPais,
			String idioma) {
		CommerceCard card = new CommerceCard();
		int idError= 0;
		String MessageError = "";
		try{
			LOGGER.debug("*************OBTENIENDO TARJETA TEBCA {} {} *************",id,idPais);
			EnrollCardRes activation = tebcaClient.CreateMCTebca(id, idApp, idPais, idioma, "COMERCIO");
			
			if(activation.getIdError() != 0){
				idError = activation.getIdError();
				MessageError = activation.getMensajeError();
			}
			LOGGER.debug("*********FINALIZANDO OBTENCION DE TARJETA TEBCA {} {}**************",id,idPais);
			
		}catch(Exception ex){
			LOGGER.error("ERROR AL OBTENER TARJETA MOBILECARD " + id, ex);
		}finally{
			card = getCommerceCard(id, idApp, idPais, idioma);
			card.setIdError(idError);
			card.setMensajeError(MessageError);
		}
		LOGGER.debug("RETORNANDO RESPUESTA " + gson.toJson(card));
		return card;
	}
	
	
	@Override
	public CommerceCard CardLockUnLock(long id, int idApp, int idPais,
			String idioma, int process) {
		CommerceCard card = new CommerceCard();
		int idError= 0;
		String MessageError = "Tarjeta " + (process == 1 ? "desbloqueada con éxito" : "bloqueada con éxito");
		McResponse chgstatus = new McResponse();
		try{
			LOGGER.debug("*************BLOQUEO / DESBLOQUEO DE TARJETA {} {} *************",id,process);
			CardCommerce commerce = mapper.getCardCommercebyCommerce(id, idApp);
			
			if(commerce != null){
				
				if(process == 0){
					if(commerce.getEstatus_tebca() == 1){
						chgstatus = tebcaClient.LockUnLockCard(id, idApp, idPais, idioma, "COMERCIO", process);
					}else{
						chgstatus.setIdError(600);
						chgstatus.setMensajeError("La tarjeta ya se encuentra bloqueada");
					}
				}else if(process == 1){
					if(commerce.getEstatus_tebca() == 0){
						chgstatus = tebcaClient.LockUnLockCard(id, idApp, idPais, idioma, "COMERCIO", process);
					}else{
						chgstatus.setIdError(600);
						chgstatus.setMensajeError("La tarjeta ya se encuentra activa");
					}
				}
			}else{
				chgstatus.setIdError(400);
				chgstatus.setMensajeError("Comercio no registrado en Mobilecard");
			}
			
			if(chgstatus.getIdError() != 0){
				idError = chgstatus.getIdError();
				MessageError = chgstatus.getMensajeError();
			}
			
			LOGGER.debug("*********FINALIZANDO OBTENCION DE TARJETA TEBCA {} {}**************",id,process);
			
		}catch(Exception ex){
			LOGGER.error("ERROR AL OBTENER TARJETA MOBILECAR " + id, ex);
			idError = 500;
			MessageError = "Error al processar peticion";
		}finally{
			card = getCommerceCard(id, idApp, idPais, idioma);
			card.setIdError(idError);
			card.setMensajeError(MessageError);
		}
		LOGGER.debug("RETORNANDO RESPUESTA " + gson.toJson(card));
		return card;
	}
	
	@Override
	public CommerceCard CardReplacement(long id, int idApp, int idPais,
			String idioma) {
		CommerceCard card = new CommerceCard();
		int idError= 0;
		String MessageError = "Solicitud de Reposición enviada";
		McResponse chgstatus = new McResponse();
		try{
			LOGGER.debug("*************REPOSICION DE TARJETA {} {} *************",id,idPais);
			CardCommerce commerce = mapper.getCardCommercebyCommerce(id, idApp);
			
			if(commerce != null){
				chgstatus = tebcaClient.CardReplacement(id, idApp, idPais, idioma, "COMERCIO");
				
			}else{
				chgstatus.setIdError(400);
				chgstatus.setMensajeError("Comercio no registrado en Mobilecard");
			}
			
			if(chgstatus.getIdError() != 0){
				idError = chgstatus.getIdError();
				MessageError = chgstatus.getMensajeError();
			}
			
			LOGGER.debug("*********FINALIZANDO OBTENCION DE TARJETA TEBCA {} {}**************",id,idPais);
			
		}catch(Exception ex){
			LOGGER.error("ERROR AL OBTENER TARJETA MOBILECAR " + id, ex);
		}finally{
			card = getCommerceCard(id, idApp, idPais, idioma);
			card.setIdError(idError);
			card.setMensajeError(MessageError);
		}
		LOGGER.debug("RETORNANDO RESPUESTA " + gson.toJson(card));
		return card;
	}
	
	@Override
	public CommerceCard AccountUpdate(AccountUpdate account,int idApp, int idPais, String idioma) {
		
		CommerceCard card = new CommerceCard();
		int idError= 0;
		String MessageError = "Cuenta actualizada";
		try{
			LOGGER.debug("*************ACTUALIZACION DE CUENTA {} {} *************",account.getId(),idPais);
			Commerce commerce = mapper.getCommerce(account.getId(), idApp);
			
			if(commerce != null){
				mapper.updateAccountCommerce(account);
				
			}else{
				idError = 400;
				MessageError = "Comercio no registrado en Mobilecard";
			}
			
			LOGGER.debug("*********FINALIZANDO OBTENCION DE TARJETA TEBCA {} {}**************",account.getId(),idPais);
			
		}catch(Exception ex){
			LOGGER.error("ERROR AL ACTUALIZAR TARJETA MOBILECARD " + account.getId(), ex);
		}finally{
			card = getCommerceCard(account.getId(), idApp, idPais, idioma);
			card.setIdError(idError);
			card.setMensajeError(MessageError);
		}
		LOGGER.debug("RETORNANDO RESPUESTA " + gson.toJson(card));
		return card;
	}
	
	@Override
	public CommerceCard AccountFavoriteUpdate(long id, int type, int idApp,
			int idPais, String idioma) {
		
		CommerceCard card = new CommerceCard();
		int idError= 0;
		String MessageError = "Favorito actualizado";
		try{
			LOGGER.debug("*************ACTUALIZACION DE FAVORITO {} {} *************",id,idPais);
			CardCommerce commerce = mapper.getCardCommercebyCommerce(id, idApp);
			
			if(commerce != null){
				mapper.updateFavoriteAccount(id, type);
				
			}else{
				idError = 400;
				MessageError = "Comercio no registrado en Mobilecard";
			}
			
			LOGGER.debug("*********FINALIZANDO OBTENCION DE TARJETA TEBCA {} {}**************",id,idPais);
			
		}catch(Exception ex){
			LOGGER.error("ERROR AL ACTUALIZAR TIPO CUENTA FAVORITO " + id, ex);
		}finally{
			card = getCommerceCard(id, idApp, idPais, idioma);
			card.setIdError(idError);
			card.setMensajeError(MessageError);
		}
		LOGGER.debug("RETORNANDO RESPUESTA " + gson.toJson(card));
		return card;
	}
	
	
	private double getBalanceTebca(long idUsuario, int idApp, int idPais, String idioma, String tipoUsuario){
		double balance = 0.0;
		try{
			
			BalanceRes resp = tebcaClient.getBalance(idUsuario, idApp, idPais, idioma, tipoUsuario);
			if(resp!= null && resp.getRc().equals("0")){
				balance = Long.parseLong(resp.getCurrentBalance());
			}
			
		}catch(Exception ex){
			LOGGER.error("ERROR AL OBTENER BALANCE", ex);
		}
		return balance;
	}
	
}
