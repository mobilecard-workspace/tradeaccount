package com.addcel.tradeaccount.spring.services;

import java.util.ArrayList;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.HttpServerErrorException;
import org.springframework.web.client.RestTemplate;
//import org.springframework.web.util.UriComponentsBuilder;
import org.springframework.web.util.UriComponentsBuilder;

import com.addcel.tradeaccount.client.tebca.AssigCardReq;
import com.addcel.tradeaccount.client.tebca.BalanceRes;
import com.addcel.tradeaccount.client.tebca.EnrollCardRes;
import com.addcel.tradeaccount.spring.model.McResponse;
import com.google.gson.Gson;

@Service
public class TebcaClient {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(TebcaClient.class);
	
	private static final String URL_GET_CARD_TEBCA = "http://localhost/Tebca/{idApp}/{idPais}/{idioma}/{tipoUsuario}/associateCardMC";
	
	private static final String URL_CREATE_CARDMC_TEBCA = "http://localhost/Tebca/{idApp}/{idPais}/{idioma}/{tipoUsuario}/assignCardMC";
	
	private static final String URL_TEBCA_CARD_LOCK_UNLOCK = "http://localhost/Tebca/{idApp}/{idPais}/{idioma}/{tipoUsuario}/chgblockstatus";
	
	private static final String PATH_ACCOUNT_REPLACEMENT = "http://localhost/Tebca/{idApp}/{idPais}/{idioma}/{tipoUsuario}/replacement";
	
	private static final String PATH_GET_BALANCE = "http://localhost/Tebca/{idApp}/{idPais}/{idioma}/{tipoUsuario}/balance";
	
	private RestTemplate restTemplate = new RestTemplate();
	
	private Gson gson = new Gson();
	
	public EnrollCardRes AssociateCard(long idUsuario, String pan,String vigencia, String codigo,int idApp,int idPais, String idioma,String tipoUsuario){
		
		EnrollCardRes resp = null;
		try{
			AssigCardReq card = new AssigCardReq();
			card.setCodigo(codigo);
			card.setIdUsuario(idUsuario);
			card.setPan(pan);
			card.setVigencia(vigencia);
			
			HttpHeaders headers = new HttpHeaders();
			headers.setContentType(MediaType.APPLICATION_JSON);
			HttpEntity<AssigCardReq> request = new HttpEntity<AssigCardReq>(card,headers);
			
			LOGGER.debug("ENVIANDO PETICION LOCAL TEBCA ..." + URL_GET_CARD_TEBCA.replace("{idApp}", idApp+"").replace("{idPais}", idPais+"")
					.replace("{idioma}" ,idioma).replace("{tipoUsuario}", tipoUsuario));
			
			resp  = (EnrollCardRes)restTemplate.postForObject(URL_GET_CARD_TEBCA.replace("{idApp}", idApp+"").replace("{idPais}", idPais+"")
					.replace("{idioma}" ,idioma).replace("{tipoUsuario}", tipoUsuario),request,EnrollCardRes.class);
			
			
			LOGGER.debug("RESPUESTA LOCAL TEBCA... " + gson.toJson(resp));
		}catch(HttpServerErrorException er){
			LOGGER.error("ERROR AL CONSUMIR TEBCA", er);
		}
		catch(Exception ex){
			LOGGER.error("ERROR AL ASOCIAR TARJETA TEBCA", ex);
		}
		return resp;
	}
	
	public EnrollCardRes CreateMCTebca(long idUsuario,int idApp,int idPais, String idioma,String tipoUsuario){
		EnrollCardRes resp = null;
		try{
			
			HttpHeaders headers = new HttpHeaders();
			headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);

			MultiValueMap<String, String> map= new LinkedMultiValueMap<String, String>();
			map.add("idUsuario", idUsuario+"");

			HttpEntity<MultiValueMap<String, String>> request = new HttpEntity<MultiValueMap<String, String>>(map, headers);

			LOGGER.debug("ENVIANDO PETICION TEBCA ...");
			ResponseEntity<String> response = restTemplate.postForEntity( URL_CREATE_CARDMC_TEBCA.replace("{idApp}", idApp+"").replace("{idPais}", idPais+"")
					.replace("{idioma}" ,idioma).replace("{tipoUsuario}", tipoUsuario), request , String.class );
			
			LOGGER.debug("RESPUESTA TEBCA " + response.getBody());
			resp  = gson.fromJson(response.getBody(), EnrollCardRes.class);
			
			
			LOGGER.debug("RESPUESTA LOCAL TEBCA... " + gson.toJson(resp));
		}catch(HttpServerErrorException er){
			LOGGER.error("ERROR AL CONSUMIR TEBCA", er);
		}
		catch(Exception ex){
			LOGGER.error("ERROR AL ASOCIAR TARJETA TEBCA", ex);
		}
		return resp;
	}
	
	
	public McResponse LockUnLockCard(long idUsuario,int idApp,int idPais, String idioma,String tipoUsuario,int proceso){
		McResponse resp = null;
		try{
			
			HttpHeaders headers = new HttpHeaders();
			headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);

			MultiValueMap<String, String> map= new LinkedMultiValueMap<String, String>();
			map.add("id", idUsuario+"");
			map.add("proceso", proceso+""); // 0 = bloquear   1=desbloquear

			HttpEntity<MultiValueMap<String, String>> request = new HttpEntity<MultiValueMap<String, String>>(map, headers);

			LOGGER.debug("ENVIANDO PETICION TEBCA ...");
			ResponseEntity<String> response = restTemplate.postForEntity( URL_TEBCA_CARD_LOCK_UNLOCK.replace("{idApp}", idApp+"").replace("{idPais}", idPais+"")
					.replace("{idioma}" ,idioma).replace("{tipoUsuario}", tipoUsuario), request , String.class );
			
			LOGGER.debug("RESPUESTA TEBCA " + response.getBody());
			resp  = gson.fromJson(response.getBody(), McResponse.class);
			
			
			LOGGER.debug("RESPUESTA LOCAL TEBCA...BLOQUEAR/DESBLOQUEAR CUENTA " + gson.toJson(resp));
		}catch(HttpServerErrorException er){
			LOGGER.error("ERROR AL BLOQUEAR/DESBLOQUEAR TARJETA TEBCA", er);
		}
		catch(Exception ex){
		}
		return resp;
	}
	
	public McResponse CardReplacement(long idUsuario,int idApp,int idPais, String idioma,String tipoUsuario){
		McResponse resp = null;
		try{
			
			HttpHeaders headers = new HttpHeaders();
			headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);

			MultiValueMap<String, String> map= new LinkedMultiValueMap<String, String>();
			map.add("id", idUsuario+"");

			HttpEntity<MultiValueMap<String, String>> request = new HttpEntity<MultiValueMap<String, String>>(map, headers);

			LOGGER.debug("ENVIANDO PETICION TEBCA REPOCISION TARJETA ...");
			ResponseEntity<String> response = restTemplate.postForEntity( PATH_ACCOUNT_REPLACEMENT.replace("{idApp}", idApp+"").replace("{idPais}", idPais+"")
					.replace("{idioma}" ,idioma).replace("{tipoUsuario}", tipoUsuario), request , String.class );
			
			LOGGER.debug("RESPUESTA TEBCA REPOCISION TARJETA " + response.getBody());
			resp  = gson.fromJson(response.getBody(), McResponse.class);
			
			
			LOGGER.debug("RESPUESTA LOCAL TEBCA... REPOCISION TARJETA" + gson.toJson(resp));
		}catch(HttpServerErrorException er){
			LOGGER.error("ERROR AL CONSUMIR TEBCA REPOCISION TARJETA ", er);
		}
		catch(Exception ex){
			LOGGER.error("ERROR EN REPOCISION TARJETA", ex);
		}
		return resp;
	}
	
	
	public BalanceRes getBalance(long idUsuario,int idApp,int idPais, String idioma,String tipoUsuario){
		BalanceRes resp = null;
		try{
			
			HttpHeaders headers = new HttpHeaders();
			headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);

			MultiValueMap<String, String> map= new LinkedMultiValueMap<String, String>();
			map.add("idUsuario", idUsuario+"");

			HttpEntity<MultiValueMap<String, String>> request = new HttpEntity<MultiValueMap<String, String>>(map, headers);

			LOGGER.debug("ENVIANDO PETICION TEBCA BALANCE ...");
			ResponseEntity<String> response = restTemplate.postForEntity( PATH_GET_BALANCE.replace("{idApp}", idApp+"").replace("{idPais}", idPais+"")
					.replace("{idioma}" ,idioma).replace("{tipoUsuario}", tipoUsuario), request , String.class );
			
			LOGGER.debug("RESPUESTA TEBCA BALANCE " + response.getBody());
			resp  = gson.fromJson(response.getBody(), BalanceRes.class);
			
			
			LOGGER.debug("RESPUESTA LOCAL TEBCA... GET BALANCE " + gson.toJson(resp));
		}catch(HttpServerErrorException er){
			LOGGER.error("ERROR AL CONSUMIR GET BALANCE ", er);
		}
		catch(Exception ex){
			LOGGER.error("ERROR EN GET BALANCE", ex);
		}
		return resp;
	}
	
	

}
