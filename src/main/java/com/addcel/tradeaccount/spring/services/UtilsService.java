package com.addcel.tradeaccount.spring.services;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
//import org.springframework.security.crypto.encrypt.Encryptors;
//import org.springframework.security.crypto.encrypt.TextEncryptor;
//import org.springframework.security.crypto.keygen.KeyGenerators;
import org.springframework.stereotype.Component;

import crypto.Crypto;

@Component
public class UtilsService {
	
    private static final Logger logger = LoggerFactory.getLogger(UtilsService.class);
	
    private static final String patron = "dd/MM/yyyy";

    private static final String TelefonoServicio = "5525963513";
    


    public static String setSMS(String Telefono) {
        return Crypto.aesEncrypt(parsePass(TelefonoServicio), Telefono);
    }

    public static String getSMS(String Telefono) {
        return Crypto.aesDecrypt(parsePass(TelefonoServicio), Telefono);
    }

    public static String parsePass(String pass) {
        int len = pass.length();
        String key = "";

        for (int i = 0; i < 32 / len; i++) {
            key += pass;
        }

        int carry = 0;
        while (key.length() < 32) {
            key += pass.charAt(carry);
            carry++;
        }
        return key;
    }
	
	
	public static boolean isEmpty(String cadena){
		boolean resp = false;
		if(cadena == null){
			resp = true;
		}else if("".equals(cadena)){
			resp = true;
		}
		return resp; 
		
	}
	
/*	public static String encryptApp(String password, String text){
		//String password = "ff39a0df-9d11-4c10-85a0-cfd16c67";  
        final String salt = "ff39a0df";//KeyGenerators.string().generateKey();
        TextEncryptor encryptor = Encryptors.text(password, salt);      
        //System.out.println("Salt: \"" + salt + "\"");
        
        //System.out.println("Original text: \"" + textToEncrypt + "\"");
        String encryptedText = encryptor.encrypt(text);
        return encryptedText;
        /*System.out.println("Encrypted text: \"" + encryptedText + "\"");
        TextEncryptor decryptor = Encryptors.text(password, salt);
        String decryptedText = decryptor.decrypt(encryptedText);
        System.out.println("Decrypted text: \"" + decryptedText + "\"");*/
           
	//}
	
	/*public static String decryptApp(String password, String text){
		
		final String salt = "ff39a0df";//KeyGenerators.string().generateKey();
		TextEncryptor decryptor = Encryptors.text(password, salt);
        String decryptedText = decryptor.decrypt(text);
        
		return decryptedText;
	}*/
	
       
}
