package com.addcel.tradeaccount.spring.services;

import com.addcel.tradeaccount.spring.model.AccountUpdate;
import com.addcel.tradeaccount.spring.model.CardReq;
import com.addcel.tradeaccount.spring.model.CommerceCard;

public interface TradeService {

	String getSaludo();
	boolean isActive(Integer idApp);
	CommerceCard getCommerceCard(long id, int idApp, int idPais, String idioma);
	CommerceCard ActivateCard(CardReq req, int idApp, int idPais, String idioma);
	CommerceCard ApplyforCard(long id, int idApp, int idPais, String idioma);
	CommerceCard CardLockUnLock(long id, int idApp, int idPais, String idioma,int process);
	CommerceCard CardReplacement(long id, int idApp, int idPais, String idioma);
	CommerceCard AccountUpdate(AccountUpdate account,int idApp, int idPais, String idioma);
	CommerceCard AccountFavoriteUpdate(long id,int type, int idApp, int idPais, String idioma);
	
}
