package com.addcel.tradeaccount.spring.model;

public class AccountUpdate {

	private long id;
	private String cuenta;
	//private String representante_nombre;
	//private String representante_paterno;
	//private String representante_materno;
	private int idBanco;
	
	public AccountUpdate() {
		// TODO Auto-generated constructor stub
	}
	
	public void setId(long id) {
		this.id = id;
	}
	
	public long getId() {
		return id;
	}

	public String getCuenta() {
		return cuenta;
	}

	public void setCuenta(String cuenta) {
		this.cuenta = cuenta;
	}

	/*public String getRepresentante_nombre() {
		return representante_nombre;
	}

	public void setRepresentante_nombre(String representante_nombre) {
		this.representante_nombre = representante_nombre;
	}

	public String getRepresentante_paterno() {
		return representante_paterno;
	}

	public void setRepresentante_paterno(String representante_paterno) {
		this.representante_paterno = representante_paterno;
	}

	public String getRepresentante_materno() {
		return representante_materno;
	}

	public void setRepresentante_materno(String representante_materno) {
		this.representante_materno = representante_materno;
	}*/

	public int getIdBanco() {
		return idBanco;
	}

	public void setIdBanco(int idBanco) {
		this.idBanco = idBanco;
	}
}
