package com.addcel.tradeaccount.spring.model;

public class AccountCommerce {

	private String cuenta;
	private String nombre;
	private int idBanco;
	private boolean favorito;
	private String nombreBanco;
	
	public AccountCommerce() {
		// TODO Auto-generated constructor stub
	}
	
	public void setNombreBanco(String nombreBanco) {
		this.nombreBanco = nombreBanco;
	}
	
	public String getNombreBanco() {
		return nombreBanco;
	}
	
	public void setFavorito(boolean favorito) {
		this.favorito = favorito;
	}
	
	public boolean isFavorito() {
		return favorito;
	}

	public void setIdBanco(int idBanco) {
		this.idBanco = idBanco;
	}
	
	public int getIdBanco() {
		return idBanco;
	}
	
	public String getCuenta() {
		return cuenta;
	}

	public void setCuenta(String cuenta) {
		this.cuenta = cuenta;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	
	
}
