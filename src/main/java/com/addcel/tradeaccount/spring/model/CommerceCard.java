package com.addcel.tradeaccount.spring.model;

public class CommerceCard extends McResponse{

	private MobileCardCard tebca;
	private AccountCommerce cci;
	
	public CommerceCard() {
		// TODO Auto-generated constructor stub
	}

	public MobileCardCard getTebca() {
		return tebca;
	}

	public void setTebca(MobileCardCard tebca) {
		this.tebca = tebca;
	}

	public AccountCommerce getCci() {
		return cci;
	}

	public void setCci(AccountCommerce cci) {
		this.cci = cci;
	}
	
	
}
