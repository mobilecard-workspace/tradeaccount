package com.addcel.tradeaccount.mybatis.model.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.addcel.tradeaccount.mybatis.model.vo.Bank;
import com.addcel.tradeaccount.mybatis.model.vo.CardCommerce;
import com.addcel.tradeaccount.mybatis.model.vo.Commerce;
import com.addcel.tradeaccount.spring.model.AccountUpdate;




public interface Mapper {

	public String isAppActive(@Param(value = "idApp") Integer idApp);
	
	public CardCommerce getCardCommercebyCommerce(@Param(value = "id") long id,@Param(value = "id_aplicacion") long id_aplicacion);
	
	public Commerce getCommerce(@Param(value = "id") long id, @Param(value = "idApp") int idApp);
	
	public void updateAccountCommerce(AccountUpdate account);
	
	public void updateFavoriteAccount(@Param(value = "id") long id, @Param(value = "type") int type);
	
	public Bank getBancoPeru(@Param(value = "idBanco") long idBanco);
	

}
