package com.addcel.tradeaccount.mybatis.model.vo;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Commerce {

	private long id;
	private String municipio;
	private String colonia;
	private String cp;
	private long id_estado;
	private String calle;  
	private Integer id_aplicacion;
	private String cuenta_clabe;
	private String nombre_prop_cuenta;
	private String representante_nombre;
	private String representante_paterno;
	private String representante_materno;
	private int id_banco;
	private int cuenta_favorito;
	private String cci;
	
	
	public Commerce() {
		// TODO Auto-generated constructor stub
	}
	
	public void setCci(String cci) {
		this.cci = cci;
	}
	
	public String getCci() {
		return cci;
	}
	
	public void setCuenta_favorito(int cuenta_favorito) {
		this.cuenta_favorito = cuenta_favorito;
	}
	
	public int getCuenta_favorito() {
		return cuenta_favorito;
	}
	
	public void setId_banco(int id_banco) {
		this.id_banco = id_banco;
	}
	
	public int getId_banco() {
		return id_banco;
	}
	
	public String getRepresentante_nombre() {
		return representante_nombre;
	}



	public void setRepresentante_nombre(String representante_nombre) {
		this.representante_nombre = representante_nombre;
	}



	public String getRepresentante_paterno() {
		return representante_paterno;
	}



	public void setRepresentante_paterno(String representante_paterno) {
		this.representante_paterno = representante_paterno;
	}



	public String getRepresentante_materno() {
		return representante_materno;
	}



	public void setRepresentante_materno(String representante_materno) {
		this.representante_materno = representante_materno;
	}



	public void setCuenta_clabe(String cuenta_clabe) {
		this.cuenta_clabe = cuenta_clabe;
	}
	
	public String getCuenta_clabe() {
		return cuenta_clabe;
	}
	
	public void setNombre_prop_cuenta(String nombre_prop_cuenta) {
		this.nombre_prop_cuenta = nombre_prop_cuenta;
	}
	
	public String getNombre_prop_cuenta() {
		return nombre_prop_cuenta;
	}

	public void setId_aplicacion(Integer id_aplicacion) {
		this.id_aplicacion = id_aplicacion;
	}
	
	public Integer getId_aplicacion() {
		return id_aplicacion;
	}
	
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getMunicipio() {
		return municipio;
	}

	public void setMunicipio(String municipio) {
		this.municipio = municipio;
	}

	public String getColonia() {
		return colonia;
	}

	public void setColonia(String colonia) {
		this.colonia = colonia;
	}

	public String getCp() {
		return cp;
	}

	public void setCp(String cp) {
		this.cp = cp;
	}

	public long getId_estado() {
		return id_estado;
	}

	public void setId_estado(long id_estado) {
		this.id_estado = id_estado;
	}

	public String getCalle() {
		return calle;
	}

	public void setCalle(String calle) {
		this.calle = calle;
	}
	
	

}
